import QtQuick
import QtQuick.Controls

Window {
    id: window

    width: 640
    height: 480
    visible: true
    title: qsTr("Hello World")

    Component.onCompleted: {
        for (let i = 0; i < 100; ++i) {
            listModel.append({"name": "Item " + i })
        }
    }

    Shortcut {
        sequence: StandardKey.Quit
        onActivated: Qt.quit()
    }


    ScrollView {
        id: scrollView

        anchors.fill: parent

        ListView {
            id: view

            width: window.width
            height: window.height

            model: ListModel {
                id: listModel
                dynamicRoles: true
            }

            delegate: SwipeDelegate {
                id: itemDelegate

                text: name
                width: view.width

                swipe.right: Label { text: "Hello World" }

                ItemDragButton {
                    id: dragButton

                    anchors {
                        verticalCenter: parent.verticalCenter
                        right: parent.right
                    }
                    item: index
                    listViewItem: itemDelegate
                    model: listModel
                }

                ReorderableListViewOverlay {
                    anchors.fill: parent
                    item: index
                    dragTile: dragButton.dragTile
                    model: listModel
                }
            }
        }
    }
}
