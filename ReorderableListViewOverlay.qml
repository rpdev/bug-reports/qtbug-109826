import QtQuick
import QtQml
import QtQuick.Controls
import QtQuick.Controls.Material

Item {
    id: root

    property var model
    property int layout: Qt.Vertical
    property var item
    property var dragTile

    QtObject {
        id: d

        readonly property var dropKeys: {
            let result = []
            if (dragTile) {
                for (let key in dragTile.Drag.mimeData) {
                    result.push(key)
                }
            }
            return result
        }
    }

    Pane {
        id: itemBackground
        visible: dragTile && dragTile.item == item && dragTile.dragging
        anchors.fill: parent
        Material.background: Material.Teal
        opacity: 0.2
        z: 9
    }

    Pane {
        width: root.layout === Qt.Vertical ? parent.width : 3
        height: root.layout === Qt.Vertical ? 3 : parent.height
        Material.background: Material.Teal
        visible: upperDropArea.containsDrag
        z: 10
        y: -1
    }

    Rectangle {
        width: root.layout === Qt.Vertical ? parent.width : 3
        y: root.layout === Qt.Vertical ? parent.height - 1 : 0
        x: root.layout === Qt.Vertical ? 0 : parent.width - 1
        height: root.layout === Qt.Vertical ? 3 : parent.height
        Material.background: Material.Teal
        visible: lowerDropArea.containsDrag
        z: 10
    }

    DropArea {
        id: upperDropArea
        anchors {
            left: parent.left
            right: root.layout === Qt.Vertical ? parent.right : parent.horizontalCenter
            top: parent.top
            bottom: root.layout === Qt.Vertical ? parent.verticalCenter : parent.bottom
        }
        keys: d.dropKeys
        onDropped: drop => {
                       drop.accept()
                       let fromItemIndex = drag.source.item
                       root.model.move(fromItemIndex, root.item, 1)
                   }
    }

    DropArea {
        id: lowerDropArea
        anchors {
            left: root.layout === Qt.Vertical ? parent.left : parent.horizontalCenter
            right: parent.right
            bottom: parent.bottom
            top: root.layout === Qt.Vertical ? parent.verticalCenter : parent.top
        }
        keys: d.dropKeys
        onDropped: drop => {
                       drop.accept()
                       let fromItemIndex = drag.source.item
                       root.model.move(fromItemIndex, root.item + 1, 1)
                   }
    }
}
