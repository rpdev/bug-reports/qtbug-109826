import QtQuick
import QtQuick.Controls

ToolButton {
    id: moveButton

    readonly property alias dragTile: dragTile
    readonly property alias dragging: dragTile.dragging

    required property var item
    required property var model
    required property var listViewItem

    text: "↕"

    visible: {
        return !!listViewItem.hovered
    }

    ItemDragTile {
        id: dragTile
        anchors.fill: parent
        item: moveButton.item
        model: moveButton.model
    }
}
