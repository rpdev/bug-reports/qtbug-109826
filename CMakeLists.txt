cmake_minimum_required(VERSION 3.16)

project(qtbug-109826 VERSION 0.1 LANGUAGES CXX)

set(CMAKE_AUTOMOC ON)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

find_package(Qt6 6.2 COMPONENTS Quick QuickControls2 REQUIRED)

qt_add_executable(appqtbug-109826
    main.cpp
)

qt_add_qml_module(appqtbug-109826
    URI qtbug-109826
    VERSION 1.0
    QML_FILES
        main.qml
        ReorderableListViewOverlay.qml
        ItemDragTile.qml
        ItemDragButton.qml
)

set_target_properties(appqtbug-109826 PROPERTIES
    MACOSX_BUNDLE_GUI_IDENTIFIER my.example.com
    MACOSX_BUNDLE_BUNDLE_VERSION ${PROJECT_VERSION}
    MACOSX_BUNDLE_SHORT_VERSION_STRING ${PROJECT_VERSION_MAJOR}.${PROJECT_VERSION_MINOR}
    MACOSX_BUNDLE TRUE
    WIN32_EXECUTABLE TRUE
)

target_link_libraries(appqtbug-109826
    PRIVATE Qt6::Quick Qt6::QuickControls2)

install(TARGETS appqtbug-109826
    BUNDLE DESTINATION .
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
